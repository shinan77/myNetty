package com.shinan.svnbranchutil.command;
/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Company: 北京九恒星科技股份有限公司</p>
 *
 * @author :shinan
 * 
 * @since：2018年1月30日 上午11:31:22
 * 
 */
public class CopyCommand implements Command{

    @Override
    public String getExecuteScript(String appNo, String projectNo) {
        return "svn copy " + getSrc(appNo) + " " + getTarget(appNo, projectNo) + " " + getMessage(projectNo);
    }
    
    public String getMessage(String projectNo) {
        return "-m \"" + projectNo + "$$创建分支......\"";
    }
    
    public String getSrc(String appNo) {
        return getUrl() + "/" + appNo + "/trunk";
    }
    
    public String getTarget(String appNo, String projectNo) {
        return getUrl() + "/" + appNo + "/branches/"+ appNo + "_" + projectNo;
    }
    
    public String getUrl() {
        return "http://192.168.0.2/svn/repo/ProD";
    }

}
