package com.shinan.svnbranchutil.command;
/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Company: 北京九恒星科技股份有限公司</p>
 *
 * @author :shinan
 * 
 * @since：2018年1月30日 上午11:18:45
 * 
 */
public interface Command {

    String getExecuteScript(String appNo, String projectNo);
}
