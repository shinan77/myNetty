package com.shinan.svnbranchutil;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import com.shinan.svnbranchutil.command.Command;
import com.shinan.svnbranchutil.command.CopyCommand;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Company: 北京九恒星科技股份有限公司</p>
 *
 * @author :shinan
 * 
 * @since：2018年1月30日 上午11:16:01
 * 
 */
public class RunCommand {
    
    public static void main(String[] args) throws IOException {
        Command command = new CopyCommand();
        Process process = Runtime.getRuntime().exec(command.getExecuteScript("SDM-Service", "TA0596"));
        InputStream resStream = process.getInputStream();
        InputStream errStream = process.getErrorStream();
        String rsMsg = getStringByStream(resStream);
        String errMsg = getStringByStream(errStream);
        System.out.println(rsMsg);
        System.out.println(errMsg);
    }

    private static String getStringByStream(InputStream resStream) throws IOException, UnsupportedEncodingException {
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int length;
        while ((length = resStream.read(buffer)) != -1) {
            result.write(buffer, 0, length);
        }
        String rsMsg = result.toString("UTF-8");
        return rsMsg;
    }

}
