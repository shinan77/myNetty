package com.shinan.nio;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Company: 北京九恒星科技股份有限公司</p>
 *
 * @author :shinan
 * 
 * @since：2017年12月25日 下午4:33:51
 * 
 */
public class BioServer implements Runnable {

    @Override
    public void run() {
        System.out.println("Hello Server!!");
        try {
            ServerSocket server = new ServerSocket(4700);
            Socket socket = socket = server.accept();
            String line;
            BufferedReader is = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            System.out.println("Client:" + is.readLine());
            PrintWriter os = new PrintWriter(socket.getOutputStream());
            line = "hello";
            os.println(line);
            os.flush();
            is.close();
            socket.close();
            server.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        
    }

}
