package com.shinan.nio;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

import org.junit.Test;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Company: 北京九恒星科技股份有限公司</p>
 *
 * @author :shinan
 * 
 * @since：2017年12月25日 下午4:39:53
 * 
 */
public class TestReactor {
    @Test
    public void testConnect() throws UnknownHostException, IOException {
        Socket s = new Socket("localhost", 4700);  
        
        InputStream inStream = s.getInputStream();  
        OutputStream outStream = s.getOutputStream();  
  
        // 输出  
        PrintWriter out = new PrintWriter(outStream, true);  
        out.println("getPublicKey你好！");  
        out.flush();  
  
        s.shutdownOutput();// 输出结束  
  
        // 输入  
        Scanner in = new Scanner(inStream);  
  
        StringBuilder sb = new StringBuilder();  
        while (in.hasNextLine()) {  
            String line = in.nextLine();  
            sb.append(line);  
        }  
  
        String response = sb.toString();  
        System.out.println("response=" + response);  
    }
    
    @Test
    public void testBioServer(){
        Thread server = new Thread(new BioServer());
        server.start();

        while(true){
            try {
                Thread.sleep(3*1000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
    
    @Test 
    public void testNioServer(){
        Thread server = new Thread(new Reactor());
        server.start();

        while(true){
            try {
                Thread.sleep(3*1000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

}
