package com.shinan.nio;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.Set;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Company: 北京九恒星科技股份有限公司</p>
 *
 * @author :shinan
 * 
 * @since：2017年12月25日 下午4:14:10
 * 
 */
public class Reactor implements Runnable {

    public int id = 100001;
 // 缓冲区大小  
    private static final int Buffer_Size = 1024;  
 // 超时时间,单位毫秒  
    private static final int TimeOut = 3000; 
    
 // 本地字符集  
    private static final String LocalCharSetName = "UTF-8";
    
    @Override
    public void run() {
        init();
        
    }
    
    public void init() {
        try {
            ServerSocketChannel socketChannel = ServerSocketChannel.open();
            InetSocketAddress inetSocketAddress = new InetSocketAddress(4700);
            socketChannel.socket().bind(inetSocketAddress);
            socketChannel.configureBlocking(false);
            Selector selector = Selector.open();
            socketChannel.register(selector, SelectionKey.OP_ACCEPT);
            System.out.println("Server started .... port:4700");
            listener(selector);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    public void listener(Selector selector) {
        try {
            while (true) {
             // 等待某个信道就绪  
                if (selector.select(TimeOut) == 0) {  
                    System.out.println(".");  
                    continue;  
                }  
                Set<SelectionKey> readySelectionKey = selector.selectedKeys();
                Iterator<SelectionKey> it = readySelectionKey.iterator();
                while (it.hasNext()) {
                    SelectionKey selectionKey = it.next();
                    if (selectionKey.isAcceptable()) {
                        System.out.println(selectionKey.attachment() + " - 接受请求事件");
                        ServerSocketChannel serverSocketChannel = (ServerSocketChannel) selectionKey
                                .channel();
                        serverSocketChannel.accept();
                        serverSocketChannel.configureBlocking(false);
                        serverSocketChannel.register(selector, SelectionKey.OP_READ, ByteBuffer.allocate(Buffer_Size));
                        System.out.println(selectionKey.attachment() + " - 已连接");
                    }
                 // 客户端有写入时  
                    if (selectionKey.isReadable()) {  
                        // 获得与客户端通信的信道  
                        SocketChannel clientChannel = (SocketChannel) selectionKey.channel();  
      
                        // 得到并重置缓冲区的主要索引值  
                        ByteBuffer buffer = (ByteBuffer) selectionKey.attachment();  
                        buffer.clear();  
      
                        // 读取信息获得读取的字节数  
                        long bytesRead = clientChannel.read(buffer);  
      
                        if (bytesRead == -1) {  
                            // 没有读取到内容的情况  
                            clientChannel.close();  
                        } else {  
                            // 将缓冲区准备为数据传出状态  
                            buffer.flip();  
                            // 将获得字节字符串(使用Charset进行解码)  
                            String receivedString = Charset  
                                    .forName(LocalCharSetName).newDecoder().decode(buffer).toString();  
      
                            // 控制台打印出来  
                            System.out.println("接收到信息:" + receivedString);  
      
                            // 准备发送的文本  
                            String sendString = "你好,客户端. 已经收到你的信息" + receivedString;  
      
                            // 将要发送的字符串编码(使用Charset进行编码)后再进行包装  
                            buffer = ByteBuffer.wrap(sendString.getBytes(LocalCharSetName));  
      
                            // 发送回去  
                            clientChannel.write(buffer);  
      
                            // 设置为下一次读取或是写入做准备  
                            selectionKey.interestOps(SelectionKey.OP_READ | SelectionKey.OP_WRITE);  
                        }  
                    }  
      
                    it.remove();  
                }  
                    
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    public static String getString(ByteBuffer buffer)
    {
        String string = "";
        try
        {
            for(int i = 0; i<buffer.position();i++){
                string += (char)buffer.get(i);
            }
            return string;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return "";
        }
    }

}
