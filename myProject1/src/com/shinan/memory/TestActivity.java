package com.shinan.memory;
/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Company: 北京九恒星科技股份有限公司</p>
 *
 * @author :shinan
 * 
 * @since：2017年12月22日 上午9:01:23
 * 
 */
public class TestActivity {

    private static OutterClass.Inner innerClass;
    
    public TestActivity() {
        OutterClass outterClass = new OutterClass();
        innerClass = outterClass.new Inner();
    }
}
